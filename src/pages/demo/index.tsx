import type { NextPage } from 'next'
import Image from 'next/image'
import { LayoutsMain } from '../../components/layouts/layouts-main.component'
import { DemoComponent } from '../../features/demo/demo.component'
import styles from '../styles/Home.module.css'

const HomePages: NextPage = () => {
    return (
        <LayoutsMain
            title='New Tempora'
            page='Create Tempora'
        >
            <DemoComponent />
        </LayoutsMain>
    )
}

export default HomePages
