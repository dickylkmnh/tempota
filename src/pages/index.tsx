import type { NextPage } from 'next'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { LayoutsMain } from '../components/layouts/layouts-main.component'
import { HomeComponent } from '../features/home/home.component'

const HomePages: NextPage = () => {
  return (
    <LayoutsMain
      page='Home'
      title='Home adalah home'
    >

      {/* <div className='relative flex left-1/2 top-1/2 align-items-center'>
        <div className="grid grid-rows-1">
          <div className='text-justify text-6xl animate-bounce font-bold bg-gradient-to-r from-green-400 to-blue-500 p-3 rounded-2xl text-white'>
            Tempota
          </div>
        </div>
      </div> */}

      <HomeComponent />
    </LayoutsMain>
  )
}

export default HomePages
