import React from 'react';

export const HomeComponent = () => {
    return (
        <div>
            <section>
                <div className="top-0 h-screen">
                    <div className="h-screen bg-gradient-to-b from-indigo-500 via-purple-500 to-pink-500"></div>
                    <div className="container mx-auto">
                        <div className="absolute top-1/3 text-8xl font-bold">
                            <div>THE</div>
                            <div>TEMPOTA.IO</div>
                            <div className="text-2xl text-right tracking-widest text-red-300">------------------------- the best of your partner</div>
                            <div className="mt-10 text-left text-sm tracking-widest">
                                <div className="p-1 w-auto hover:text-amber-400 cursor-pointer">Create your demo text with Tempota.io</div>
                                <div className="mt-4 w-auto p-1 hover:text-amber-400 cursor-pointer">Start with your money and starting</div>
                            </div>
                        </div>
                        <div className="absolute top-1/4 text-6xl font-bold img_home_panda">
                            <img width="90%" src="https://cdn-icons-png.flaticon.com/512/7017/7017494.png" alt="panda" />
                        </div>
                        <div className="absolute top-1/4 text-6xl font-bold img_home_basket animate-bounce">
                            <img width="50%" src="https://cdn-icons-png.flaticon.com/512/4205/4205302.png" alt="panda" />
                        </div>
                        <div className="absolute top-1/4 text-6xl font-bold img_home_camera">
                            <img width="80%" src="https://cdn-icons-png.flaticon.com/512/8043/8043043.png" alt="panda" />
                        </div>
                    </div>
                </div>
            </section >

            <section>
                <div className="top-0 h-screen bg-pink-500">
                    <div className="container mx-auto">
                        <div className="text-3xl text-center font-bold underline">Join With Me</div>
                        <div className="grid grid-cols-4 gap-4 mt-10">
                            <div className="h-96 rounded-xl bg-gradient-to-b from-purple-600 via-white to-white shadow-2xl">
                                <div className="flex justify-center p-4">
                                    <img className="w-12 h-12" src="https://cdn-icons-png.flaticon.com/512/5828/5828065.png" alt="" />
                                </div>
                                <div className="pt-4">
                                    <div className="text-center font-semibold text-xl">easy</div>
                                    <div className="text-justify text-sm pt-2 p-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet reiciendis eius ipsum nam, ipsam voluptas. Molestias ex nihil id iusto facere optio possimus enim cumque, dolor veniam, minima, dolorum totam. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores veniam officia corrupti sequi ipsam magnam, veritatis ducimus consequuntur odio itaque facilis accusamus a ea beatae harum eum. Magni, soluta doloribus.</div>
                                </div>
                            </div>

                            <div className="h-96 rounded-xl bg-gradient-to-b from-purple-600 via-white to-white shadow-2xl">
                                <div className="flex justify-center p-4">
                                    <img className="w-12 h-12" src="https://cdn-icons-png.flaticon.com/512/1455/1455330.png" alt="" />
                                </div>
                                <div className="pt-4">
                                    <div className="text-center font-semibold text-xl">medium</div>
                                    <div className="text-justify text-sm pt-2 p-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum quae autem quod incidunt odit iusto nesciunt praesentium nostrum, expedita repudiandae, minus alias perspiciatis sunt illum dolorum quam vero beatae maiores! Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nesciunt eveniet quasi eum quos illum laudantium molestias eaque asperiores! Est asperiores dignissimos minima suscipit quidem quis rerum eos, voluptatum quaerat adipisci.</div>
                                </div>
                            </div>

                            <div className="h-96 rounded-xl bg-gradient-to-b from-purple-600 via-white to-white shadow-2xl">
                                <div className="flex justify-center p-4">
                                    <img className="w-12 h-12" src="https://cdn-icons-png.flaticon.com/512/2405/2405334.png" alt="" />
                                </div>
                                <div className="pt-4">
                                    <div className="text-center font-semibold text-xl">hard</div>
                                    <div className="text-justify text-sm pt-2 p-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa impedit, quidem repellat quos voluptatem magni eveniet ducimus officia labore laboriosam quo nihil aspernatur qui totam aliquid dolor autem consequuntur! Dolore. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id cupiditate, tenetur earum numquam error cumque quisquam labore adipisci similique inventore reiciendis repellendus veniam porro consequatur. Alias hic nulla vitae dolores!</div>
                                </div>
                            </div>

                            <div className="h-96 rounded-xl bg-gradient-to-b from-purple-600 via-white to-white shadow-2xl">
                                <div className="flex justify-center p-4">
                                    <img className="w-12 h-12" src="https://cdn-icons-png.flaticon.com/512/7626/7626213.png" alt="" />
                                </div>
                                <div className="pt-4">
                                    <div className="text-center font-semibold text-xl">expert</div>
                                    <div className="text-justify text-sm pt-2 p-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate labore molestiae, rerum nostrum earum assumenda nulla quaerat dolorem soluta porro fuga fugit aperiam molestias obcaecati nam mollitia nobis id non.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        </div >
    )
}