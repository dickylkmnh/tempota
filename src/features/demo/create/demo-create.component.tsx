import React from 'react';

import axios from 'axios';

import { DemoFormComponent } from '../form/demo-form.component';

export const DemoCreateComponent = ({ onClose }: IDemoCreate) => {
    function onSubmit(payload: ICreateForm) {
        axios.post(`${process.env.API_MAIN}/data`, payload)
            .then((res) => {
                console.log(res)
            })
            .catch((err) => {
                console.log(err)
                return err;
            })
    }

    return (
        <DemoFormComponent
            onClose={onClose}
            onSubmit={(value) => onSubmit(value)}
        />
    )

}