interface IDemoForm {
    onClose: () => void;
    onSubmit: (value) => void
}

interface IDemoCreate {
    onClose: () => void
}

interface ICreateForm {
    name: string;
    fullName: string;
}