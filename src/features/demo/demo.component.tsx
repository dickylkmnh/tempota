import React, { useState } from 'react';
import { ButtonMoleculeComponent } from 'src/components/ui/molecules/button/button-molecule.component';
import { InputMoleculeComponent } from 'src/components/ui/molecules/input/input-molecule.component';
import { ModalMoleculeComponent } from 'src/components/ui/molecules/modal/modal-molecule.component';
import { DemoCreateComponent } from './create/demo-create.component';
import { DemoFormComponent } from './form/demo-form.component';

export const DemoComponent = () => {
    const [isVisible, setIsVisible] = useState(false);

    return (
        <div className="p-3">
            <ButtonMoleculeComponent
                label="Open Dialog"
                onClick={() => setIsVisible(true)}
            />
            <ModalMoleculeComponent
                show={isVisible}
                width="max-w-2xl"
                header={(
                    <span>Create Form</span>
                )}
                body={(
                    <DemoCreateComponent onClose={() => setIsVisible(false)} />
                )}
            // footer={(
            //     <div>
            //         <button
            //             type="button"
            //             onClick={() => setIsVisible(false)}
            //             className="rounded-md bg-black bg-opacity-20 px-4 py-2 text-sm font-medium text-white hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
            //         >
            //             Close
            //         </button>

            //         <button
            //             type="button"
            //             onClick={() => setIsVisible(false)}
            //             className="rounded-md bg-black bg-opacity-20 px-4 py-2 text-sm font-medium text-white hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
            //         >
            //             Create
            //         </button>
            //     </div>
            // )}
            />
        </div>
    )
}