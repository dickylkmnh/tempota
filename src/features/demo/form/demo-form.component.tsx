import React from 'react';

import * as Yup from 'yup';
import { useFormik } from 'formik';
import { InputMoleculeComponent } from 'src/components/ui/molecules/input/input-molecule.component';
import { ButtonMoleculeComponent } from 'src/components/ui/molecules/button/button-molecule.component';

export const DemoFormComponent = ({ onClose, onSubmit }: IDemoForm) => {

    const formik = useFormik({
        initialValues: {
            name: '',
            fullName: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().required('Required'),
            fullName: Yup.string().required('Required')
        }),
        onSubmit: (e) => onSubmit(e)
    });

    return (
        <form onSubmit={formik.handleSubmit}>
            <div>
                <label className="text-sm text-gray-700" htmlFor="name">Name</label>
                <InputMoleculeComponent
                    id="name"
                    name="name"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.name}
                />
                <span className="text-xs text-red-500">{formik.errors.name}</span>
            </div>
            <div className="mt-2">
                <label className="text-sm text-gray-700" htmlFor="fullName">FullName</label>
                <InputMoleculeComponent
                    id="fullName"
                    name="fullName"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.fullName}
                />
                <span className="text-xs text-red-500">{formik.errors.fullName}</span>
            </div>

            <div className="mt-4 text-right">
                <ButtonMoleculeComponent
                    label="Close"
                    onClick={() => onClose()}
                    className="mr-2"
                    backgroundColor="bg-red-500"
                />
                <ButtonMoleculeComponent
                    label="Create"
                    type="submit"
                    backgroundColor="bg-blue-500"
                />
            </div>
        </form>
    )
}