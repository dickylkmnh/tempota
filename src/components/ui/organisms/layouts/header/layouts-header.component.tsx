import React, { Fragment } from 'react';

import { useRouter } from 'next/router';
import { Disclosure, Menu, Transition } from '@headlessui/react'

const Icons = () => (
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
        <path strokeLinecap="round" strokeLinejoin="round" d="M4.26 10.147a60.436 60.436 0 00-.491 6.347A48.627 48.627 0 0112 20.904a48.627 48.627 0 018.232-4.41 60.46 60.46 0 00-.491-6.347m-15.482 0a50.57 50.57 0 00-2.658-.813A59.905 59.905 0 0112 3.493a59.902 59.902 0 0110.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.697 50.697 0 0112 13.489a50.702 50.702 0 017.74-3.342M6.75 15a.75.75 0 100-1.5.75.75 0 000 1.5zm0 0v-3.675A55.378 55.378 0 0112 8.443m-7.007 11.55A5.981 5.981 0 006.75 15.75v-1.5" />
    </svg>
)

const navigation = [
    { title: 'Home', link: '/', icon: <Icons /> },
    { title: 'Documentation', link: '/documentation', icon: <Icons /> },
    { title: 'Demo', link: '/demo', icon: <Icons /> }
]

export const LayoutsHeaderComponent: React.FC = () => {
    const router = useRouter();

    function classNames(...classes: any) {
        return classes.filter(Boolean).join(' ')
    }
    return (
        <Disclosure as="nav" className="bg-transparent fixed w-full top-0 z-10">
            {({ open }) => (
                <>
                    <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                        <div className="relative flex h-16 items-center justify-between">
                            <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                                <div className="flex flex-shrink-0 items-center p-1 rounded-md text-sm cursor-pointer bg-gradient-to-r from-cyan-500 to-blue-500">
                                    <img
                                        className="block w-6 h-6"
                                        src="https://cdn-icons-png.flaticon.com/512/861/861106.png"
                                        alt="Your Company"
                                        width="90%"
                                    />
                                    <span className="text-lg ml-1 text-white font-bold">tempota.io</span>
                                </div>
                                <div className="hidden sm:ml-6 sm:block">
                                    <div className="flex space-x-4">
                                        {navigation.map((navigation, index) => (
                                            <a
                                                key={index}
                                                onClick={() => router.push(navigation.link)}
                                                className={classNames(
                                                    router.asPath === navigation.link ? 'text-black' : 'text-gray-300 hover:text-white',
                                                    'px-3 py-2 rounded-md text-sm font-medium cursor-pointer'
                                                )}
                                                aria-current={router.asPath === navigation.link ? 'page' : undefined}
                                            >
                                                {navigation.title}
                                            </a>
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                                <button
                                    type="button"
                                    className="rounded-full bg-gray-800 p-1 text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800"
                                >
                                    <span className="sr-only">View notifications</span>
                                    <Icons />
                                </button>
                            </div>
                        </div>
                    </div>
                </>
            )}
        </Disclosure>
        // <nav className="sticky top-0 z-10 bg-transparent">
        //     <div className="max-w-5xl mx-auto px-4">
        //         <div className="flex items-center justify-between h-16">
        //             <span className="text-2xl text-gray-900 font-semibold">Logo</span>
        //             <div className="flex space-x-4 text-gray-900">
        //                 <a href="#">Dashboard</a>
        //                 <a href="#">About</a>
        //                 <a href="#">Projects</a>
        //                 <a href="#">Contact</a>
        //             </div>
        //         </div>
        //     </div>
        // </nav>
    )
}