import { ReactNode } from "react";

declare interface ModalProps {
    header?: ReactNode;
    body?: ReacftNode;
    footer?: ReactNode;
    show: boolean;
    width?: string;
}