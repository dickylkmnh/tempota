import React from 'react';

import { InputProps } from './input-molecule';

export const InputMoleculeComponent = (props: InputProps) => {
    return (
        <input
            {...props}
            className={props.className + ' block w-full text-gray-500 rounded-md border border-blue-400 p-2 focus:outline outline-offset-1 outline-cyan-300 sm:text-sm'}
        />
    )
}