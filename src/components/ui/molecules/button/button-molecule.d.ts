import React, { ButtonHTMLAttributes } from "react";

export declare interface ButtonProps extends React.ButtonHTMLAttributes<ButtonHTMLAttributes> {
    label?: string;
    backgroundColor?: string;
    outlineColor?: string;
}