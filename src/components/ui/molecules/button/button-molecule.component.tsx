import React from 'react';

import { ButtonProps } from './button-molecule';

export const ButtonMoleculeComponent = (props: ButtonProps) => {
    return (
        <button
            {...props}
            className={props.className + ` rounded-md ${props.backgroundColor || 'bg-slate-500'} px-4 py-2 text-sm font-medium text-white hover:opacity-50 focus:outline outline-offset-0 outline-2 ${props.outlineColor || 'outline-slate-300'}`}
        >
            {props.label}
        </button>
    )
}
