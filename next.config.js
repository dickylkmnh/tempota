/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    API_MAIN: process.env.NODE_ENV === 'production' ? 'https://tempota.herokuapp.com' : 'https://tempota.herokuapp.com'
  }
}

module.exports = nextConfig
